package org.jinghouyu.http.proxy.exception;

import java.io.IOException;

/**
 * Http Exception
 * @author liujingyu
 *
 */
public class HttpException extends IOException {

	private static final long serialVersionUID = 3470774928380310426L;

	public HttpException() {
		super();
	}

	public HttpException(String message, Throwable cause) {
		super(message, cause);
	}

	public HttpException(String message) {
		super(message);
	}

	public HttpException(Throwable cause) {
		super(cause);
	}
}
