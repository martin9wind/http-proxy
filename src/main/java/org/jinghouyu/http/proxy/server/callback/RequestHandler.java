package org.jinghouyu.http.proxy.server.callback;

import java.io.IOException;

import org.jinghouyu.http.proxy.server.Request;

/**
 * provide a way for users to modify the headers/content of request
 * @author liujingyu
 *
 */
public interface RequestHandler {

	/**
	 * this instance could intercept the request
	 * if matches, method handle will be invoked. 
	 * @param request
	 * @return
	 */
	public boolean match(Request request);
	
	/**
	 * users can modify request or get all datas of this request.
	 * @param request
	 * @return
	 */
	public Request handle(Request request) throws IOException;
}