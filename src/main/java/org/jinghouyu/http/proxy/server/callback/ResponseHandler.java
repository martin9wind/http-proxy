package org.jinghouyu.http.proxy.server.callback;

import java.io.IOException;

import org.jinghouyu.http.proxy.server.Request;
import org.jinghouyu.http.proxy.server.Response;

/**
 * provide a way to users to modify response
 * @author liujingyu
 *
 */
public interface ResponseHandler {

	/**
	 * test if this instance could handle the response.
	 * if matches, method handle will be in
	 * @param request
	 * @param response
	 * @return
	 */
	public boolean match(Request request, Response response);
	
	public Response handle(Request request, Response response) throws IOException;
}
