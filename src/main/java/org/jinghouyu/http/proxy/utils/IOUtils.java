package org.jinghouyu.http.proxy.utils;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;

/**
 * 
 * @author liujingyu
 *
 */
public class IOUtils {

	public static void closeQuietly(AutoCloseable closeable) {
		if(closeable != null) {
			try {
				closeable.close();
			} catch (Exception e) {
			}
		}
	}
	
	public static void copy(InputStream in, OutputStream out) throws IOException {
		byte[] data = new byte[1024 * 100];
		while(true) {
			int len = in.read(data);
			if(len == -1) break;
			out.write(data, 0, len);
		}
		out.flush();
	}
}
