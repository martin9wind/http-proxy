package org.jinghouyu.http.proxy.utils;

import java.io.IOException;
import java.io.InputStream;

/**
 * parse an input stream
 * @author liujingyu
 *
 */
public class ParseUtils {

	public static byte[] readLine(InputStream in) throws IOException {
		ByteAppender appender = new ByteAppender();
		int _data = -1;
		while(true) {
			int data = in.read();
			if(data == -1) break;
			if(_data == '\r') {
				if(data == '\n') break;
				else {
					appender.append((byte)_data);
					appender.append((byte)data);
				}
			}
			if(data != '\r') {
				appender.append((byte) data);
			}
			_data = data;
		}
		return appender.getDatas();
	}
}
