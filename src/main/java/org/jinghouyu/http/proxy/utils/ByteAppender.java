package org.jinghouyu.http.proxy.utils;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.InputStream;

/**
 * 
 * @author liujingyu
 *
 */
public class ByteAppender {
	
	private ByteArrayOutputStream out = new ByteArrayOutputStream();
	
	public void append(byte b) {
		out.write(b);
	}
	
	public void append(byte[] bs) {
		out.write(bs, 0, bs.length);
	}
	
	public void append(byte[] bs, int pos, int length) {
		out.write(bs, pos, length);
	}
	
	public byte[] getDatas() {
		return out.toByteArray();
	}
	
	public InputStream toInputStream() {
		return new ByteArrayInputStream(getDatas());
	}
}
