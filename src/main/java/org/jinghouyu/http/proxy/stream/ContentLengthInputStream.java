package org.jinghouyu.http.proxy.stream;

import java.io.IOException;
import java.io.InputStream;

/**
 * specify a length of the stream.
 * 
 * @author liujingyu
 *
 */
public class ContentLengthInputStream extends InputStream {

	private int contentLength = 0;
	private InputStream in = null;
	private int hasReaded = 0;
	
	public ContentLengthInputStream(int contentLength, InputStream in) {
		this.contentLength = contentLength;
		this.in = in;
	}

	@Override
	public int read() throws IOException {
		if(hasReaded >= contentLength) return -1;
		int data = in.read();
		hasReaded++;
		if(data == -1) return -1;
		return data;
	}
}

