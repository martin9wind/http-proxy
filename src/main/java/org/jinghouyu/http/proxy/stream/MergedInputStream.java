package org.jinghouyu.http.proxy.stream;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.SequenceInputStream;
import java.io.UnsupportedEncodingException;

/**
 * merge a string (as the head) with an input stream.
 * @author liujingyu
 *
 */
public class MergedInputStream extends InputStream {

	private SequenceInputStream in = null;
	
	public MergedInputStream(InputStream in, String header) {
		try {
			this.in = new SequenceInputStream(new ByteArrayInputStream(header.getBytes("utf-8")), in);
		} catch (UnsupportedEncodingException e) {
			throw new RuntimeException(e);
		}
	}

	public int read() throws IOException {
		return in.read();
	}

	public int read(byte[] b) throws IOException {
		return in.read(b);
	}

	public int read(byte[] b, int off, int len) throws IOException {
		return in.read(b, off, len);
	}

	public long skip(long n) throws IOException {
		return in.skip(n);
	}

	public int available() throws IOException {
		return in.available();
	}

	public void close() throws IOException {
		in.close();
	}

	public synchronized void mark(int readlimit) {
		in.mark(readlimit);
	}

	public synchronized void reset() throws IOException {
		in.reset();
	}

	public boolean markSupported() {
		return in.markSupported();
	}
}
