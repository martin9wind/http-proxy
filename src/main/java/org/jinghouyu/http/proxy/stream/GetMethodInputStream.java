package org.jinghouyu.http.proxy.stream;

import java.io.IOException;
import java.io.InputStream;

/**
 * no data should be read.
 * @author liujingyu
 *
 */
public class GetMethodInputStream extends InputStream {

	public GetMethodInputStream() {
	}
	
	@Override
	public int read() throws IOException {
		return -1;
	}
}
